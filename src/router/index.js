import Vue from 'vue'
import VueRouter from 'vue-router'

import Index from '@/views/index'
import newCard from '@/views/newCard'


Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'Index',
    redirect: {name: 'Dashboard'}
  },
  {
    path: '/dashboard',
    name: 'DashboardWrapper',
    component: {
      render(c) {
        return c("router-view");
      }
    },
    children: [
      {
        path: '',
        name: 'Dashboard',
        component: Index,
      },
      {
        path: 'new-card',
        name: 'CreateCard',
        component: newCard
      }
    ]
  },

]

const router = new VueRouter({
  routes
})

export default router
