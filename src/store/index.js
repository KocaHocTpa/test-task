import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)
const storage = window.localStorage;
const STORAGE_KEY = 'dashboard'
export default new Vuex.Store({
  state: {
    cardsList: [
      {
        name: 'Default card 1',
        description: 'Description to default card 1'
      },
      {
        name: 'Default card 2',
        description: 'Description to default card 1'
      }
    ],
    dragNDrop: false,
  },
  mutations: {
    addCard: (state, payload) => {
      state.cardsList.push(payload);
    },
    removeCard: (state, payload) => {
      state.cardsList.splice(payload.pos, 1);
    },
    insertCard: (state, payload) => {
       state.cardsList.splice(payload.pos, 0, payload.data);
    },
    changeDragNDrop: (state, payload) => {
      state.dragNDrop = payload;
    },
    initState: (state, payload) => {
      state.cardsList = payload.cardList ?  payload.cardList: state.cardsList;
      state.dragNDrop = payload.dragNDrop ? payload.dragNDrop : state.dragNDrop;
    }
  },
  actions: {

    addCard: ({commit, dispatch}, payload) => {
      commit('addCard', payload);
      dispatch('writeToLocalStorage');
    },
    changeDragNDrop: ({commit, dispatch}, payload) => {
     commit('changeDragNDrop', payload);
     dispatch('writeToLocalStorage');
    },
    updateCardList: ({commit, dispatch, getters}, payload) => {
      let item = getters['getCardById'](payload.from);
      commit("removeCard", {pos: payload.from});
      commit("insertCard", {pos: payload.to, data: item});
      dispatch('writeToLocalStorage');
    },


    writeToLocalStorage: ({state}) => {
      storage.setItem(STORAGE_KEY, JSON.stringify({cardList: state.cardsList, dragNDrop: state.dragNDrop}))
    },
    readFromLocalStorage: ({commit,state}) => {
      let data = JSON.parse(storage.getItem(STORAGE_KEY));
      if(data === null){
        commit('initState', {})
      }else{
        commit('initState', {cardList: data.cardList, dragNDrop: data.dragNDrop})
      }
    }
  },
  getters: {
    cardList: state => state.cardsList,

    getCardById: state => id => {
      return state.cardsList[id]
    },
    getDragNDrop: state => state.dragNDrop
  },
  modules: {
  }
})
